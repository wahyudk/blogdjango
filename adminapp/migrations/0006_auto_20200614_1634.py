# Generated by Django 3.0.7 on 2020-06-14 09:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adminapp', '0005_post_headline'),
    ]

    operations = [
        migrations.AlterField(
            model_name='post',
            name='headline',
            field=models.CharField(choices=[('Yes', 'Yes'), ('No', 'No')], max_length=200, null=True),
        ),
    ]
