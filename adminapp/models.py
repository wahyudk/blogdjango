from django.db import models
from django.contrib.auth.models import User
from ckeditor.fields import RichTextField
from .utils import *
from django.db.models.signals import pre_save

# Create your models here.

class Kategori(models.Model):
	name = models.CharField(max_length=200, null=True)

	def __str__(self):
		return self.name

class Tag(models.Model):
	name = models.CharField(max_length=200, null=True)

	def __str__(self):
		return self.name

class Post(models.Model):
	STATUS = (
		('Public', 'Public'),
		('Private', 'Private'),)
	HEADLINE = (
		('Yes', 'Yes'),
		('No', 'No'),)
	author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, blank=True)
	judul = models.CharField(max_length=35, null=True)
	slug = models.SlugField(max_length=250, null=True, blank=True)
	isi = RichTextField(null=True, blank=True)
	kategori = models.ForeignKey(Kategori, null=True, on_delete=models.SET_NULL)
	tag = models.ManyToManyField(Tag)
	status = models.CharField(max_length=200, null=True, choices=STATUS)
	headline = models.CharField(max_length=200, null=True, choices=HEADLINE)
	thumbnail = models.ImageField(default="noimage2.png", null=True, blank=True)
	created_at = models.DateTimeField(auto_now_add=True, null=True)

	def __str__(self):
		return self.judul




class Comment(models.Model):
	email = models.EmailField(null=True)
	comment = models.TextField(null=True)
	post = models.ForeignKey(Post, null=True, on_delete=models.SET_NULL)
	created_at = models.DateTimeField(auto_now_add=True, null=True)

def slug_generator(sender, instance, *args, **kwargs):
	if not instance.slug:
		instance.slug = unique_slug_generator(instance)

pre_save.connect(slug_generator, sender=Post)