from django.urls import path

from django.contrib.auth import views as auth_views

from . import views
urlpatterns = [
    path('', views.home, name="home"),
    path('post/<slug:slug>-<str:pk>', views.detailpost, name="postdetail"),
    path('tag/<slug:tag>', views.bytag, name="bytag"),
    path('author/<slug:author>', views.byauthor, name="author"),
    path('places', views.placesPage, name="places"),
    path('photography', views.photographyPages, name="photography"),
]