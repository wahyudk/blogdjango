from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.paginator import Paginator
from .models import *
from .forms import *

# Create your views here.
def home(request):
	lastcontent = Post.objects.filter(headline="Yes")
	lastcount = lastcontent.count()
	lastrange = range(0, lastcount)
	oneid = lastcontent.all()[:1]
	recplaces = Post.objects.filter(kategori=5).order_by("-created_at")[:3]
	recphotography = Post.objects.filter(kategori=6).order_by("-created_at")[:3]
	# paginator = Paginator(recpost, 6)

	# page_number = request.GET.get('page')
	# page_obj = paginator.get_page(page_number)

	context = {'recphotography':recphotography,'recplaces':recplaces, 'lastcontent':lastcontent, 'oneid':oneid, 'lastrange':lastrange, 'nbar':'home', 'mode':'dark'}

	return render(request, 'admin/home.html', context)

def detailpost(request, slug, pk):
	post = Post.objects.filter(slug=slug)
	post2 = Post.objects.get(id=pk)
	judulpost = post2.judul
	comment = Comment.objects.filter(post=pk).order_by("-created_at")
	cocount = comment.count()
	form = CommentForm(initial={'post':post2})

	if request.method == 'POST':
        # print('Printing post: ', request.POST)
		formset = CommentForm(request.POST)
		if formset.is_valid():
			formset.save()
			return redirect('/post/'+pk+'#comments')

	context = {'judulpost':judulpost, 'post': post, 'nbar':'detailpost', 'mode':'light', 'comment': comment, 'cocount':cocount, 'form':form}

	return render(request, 'admin/detailpostnew.html', context)

def bytag(request, tag):
	tag = Tag.objects.get(name = tag)
	post = Post.objects.filter(tag = tag.id)
	paginator = Paginator(post, 6)

	page_number = request.GET.get('page')
	page_obj = paginator.get_page(page_number)

	context = {'post': post, 'mode':'light', 'tag':tag, 'page_obj':page_obj}

	return render(request, 'admin/landingtag.html', context)

def placesPage(request):
	places = Post.objects.filter(kategori = 5)
	paginator = Paginator(places, 6)

	page_number = request.GET.get('page')
	page_obj = paginator.get_page(page_number)

	context = {'page_obj':page_obj, 'places':places, 'nbar':'places', 'mode':'light', 'judulpost':'Places'}

	return render(request, 'admin/kategori.html', context)

def photographyPages(request):
	places = Post.objects.filter(kategori = 6)
	paginator = Paginator(places, 6)

	page_number = request.GET.get('page')
	page_obj = paginator.get_page(page_number)

	context = {'page_obj':page_obj, 'places':places, 'nbar':'photography', 'mode':'light', 'judulpost':'Photography'}

	return render(request, 'admin/kategori.html', context)

def byauthor(request, author):
	iduser = User.objects.get(username = author)
	author = Post.objects.filter(author = iduser.id)
	# content = Post.objects.filter(author=author)
	# author1 = User.objects.filter(id=author)
	paginator = Paginator(author, 6)

	page_number = request.GET.get('page')
	page_obj = paginator.get_page(page_number)

	context = {'author':author, 'page_obj':page_obj, 'mode':'light', 'iduser':iduser}
	return render(request, 'admin/author.html', context)